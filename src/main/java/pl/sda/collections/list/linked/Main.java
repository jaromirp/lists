package pl.sda.collections.list.linked;

public class Main {
    public static void main(String[] args) {
        ListLinked listLinked = new ListLinked();

        listLinked.add(1);
        listLinked.add(2);
        listLinked.add(3);
        listLinked.add(4);
        listLinked.add(5);
        listLinked.add(6);
        listLinked.add(7);

        System.out.println("-------------");
        System.out.println(listLinked.indexOf(1));
        System.out.println(listLinked.indexOf(2));
        System.out.println(listLinked.indexOf(3));
        System.out.println(listLinked.indexOf(4));

        System.out.println("-------------");
        System.out.println(listLinked);

        listLinked.remove(5);
        System.out.println("-------------");
        System.out.println(listLinked);

        listLinked.remove(5);
        System.out.println("-------------");
        System.out.println(listLinked);

        listLinked.remove(0);
        System.out.println("-------------");
        System.out.println(listLinked);

        listLinked.remove(3);
        System.out.println("-------------");
        System.out.println(listLinked);

    }
}
